package com.ncamc.controlleer;

import com.ncamc.entity.User;
import com.ncamc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class UserController {

    @Autowired
    private UserService userService;

    //添加
    @GetMapping("/save")
    public void save(){
        User user = new User();
        user.setId(System.currentTimeMillis());
        user.setName("test");
        user.setAge(15);
        userService.save(user);
    }

    //查询
    @GetMapping("/findAll")
    public Iterable<User> fandAll(){
        Iterable<User> users = userService.findAll();
        for (User user : users) {
            System.out.println(user);
        }
        return users;
    }

    @GetMapping("/del")
    //删除
    public void del(){
        userService.deleteAll();
    }

}