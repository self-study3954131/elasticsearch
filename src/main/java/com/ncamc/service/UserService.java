package com.ncamc.service;

import com.ncamc.entity.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends ElasticsearchRepository<User,Long> {
}