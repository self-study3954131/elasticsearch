package com.ncamc.entity;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

@Data
@Document(indexName = "user")
public class User implements Serializable {
    private static final long serializableUID = 551589397635941750L;

    private Long id;

    private String name;

    private int age;

}